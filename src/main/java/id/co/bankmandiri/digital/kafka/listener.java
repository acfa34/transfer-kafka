package id.co.bankmandiri.digital.kafka;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.samskivert.mustache.Mustache;
import com.samskivert.mustache.Template;
import id.co.bankmandiri.digital.everest.JSONDoc;
import id.co.bankmandiri.digital.everest.SOAClient;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Service
public class listener {
    SOAClient soaClient = new SOAClient();
    @KafkaListener(topics = "test")
    public void consume(String message) throws  Exception {
         soaClient.post("/OmniFundTransferOverBooking/1.0/fundTransferOverBooking", "upstream/soa-transfer-overbooking.json", payload)
                .map(content -> {try {

                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                    JSONDoc.extract(content,
                            Map.of("responseCode", "$.fundTransferOverBookingOmniResponse.payload.responseCode",
                                    "responseMessage", "$.fundTransferOverBookingOmniResponse.payload.responseMessage"));
                } );
        String url = "https://fcm.googleapis.com/fcm/send";
        //set up the basic authentication header
        String authorizationHeader = "key=AIzaSyApXDLnxSHvfv-hSsTFb0AlwSk_QOgEjRM";

        //setting up the request headers
        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.add("Authorization", authorizationHeader);
        requestHeaders.setContentType(MediaType.APPLICATION_JSON);
        Map<String, Object> payload = new HashMap<>();
        payload.put("transfer","/topics/all");
        payload.put("body", "1" );
//        RequestFirebase requestFirebase = new RequestFirebase();
//        requestFirebase.setTo("eiS29qYhjqc:APA91bETnS8_cuErJwDWfA74pZe6H5I2vQIVYM1MC_GfjrECp7_AcDziUW0P-Bb-MQocFYW9P8WckzLulnjWNCUpSSSLjmJHULxxY28eN0C8GUYRz3cquDtwIunYeWyc_Q9xiz8ix_36");
//        requestFirebase.setNotification(new Notification("Testing","Fauzi"));
//        File templateFile = ResourceUtils.getFile("classpath:upstream/firebase.json");
        ClassPathResource templateFile  = new ClassPathResource("upstream/firebase.json");
        Template tmpl = Mustache.compiler().compile(new String(FileCopyUtils.copyToByteArray(templateFile.getInputStream())));
//        ClassPathResource templateFile  = new ClassPathResource("templates/firebase.json");
//        StringWriter sw = new StringWriter();
//        Velocity.evaluate(new VelocityContext(payload), sw,"test", new String(
//                FileCopyUtils.copyToByteArray(templateFile.getInputStream())
//        ));


        HttpEntity<Object> requestEntity = new HttpEntity<>(tmpl.execute(payload), requestHeaders);
        DocumentContext json = null;


        json = JsonPath.parse(new RestTemplate().exchange(url,
                HttpMethod.POST,
                requestEntity,
                String.class)
                .getBody());



        Map responseCode =  json.read("$");
    }
}
